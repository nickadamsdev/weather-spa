const axios = require('axios');
const API_KEY = require('./API_KEY');

const newDays = () => {
  const days = [];
  for (let i = 0; i < 5; i += 1) {
    days[i] = {
      date: null,
      icons: {},
      maxTemp: null,
      minTemp: null,
    };
  }
  return days;
};

const getDate = (dataset) => {
  const months = ['none', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  const month = months[Number(dataset.dt_txt.slice(5, 7))];
  const day = dataset.dt_txt.slice(8, 10);
  return `${month} ${day}`;
};

const updateTemps = (days, dataset, i) => {
  const daysWithTemps = Object.assign([], days);
  if (daysWithTemps[i].maxTemp === null || dataset.main.temp_max > daysWithTemps[i].maxTemp) {
    daysWithTemps[i].maxTemp = Math.round(dataset.main.temp_max);
  }
  if (daysWithTemps[i].minTemp === null || dataset.main.temp_min < daysWithTemps[i].minTemp) {
    daysWithTemps[i].minTemp = Math.round(dataset.main.temp_min);
  }
  return daysWithTemps;
};

const updateIcons = (days, dataset, i) => {
  const daysWithIcons = Object.assign([], days);
  dataset.weather.forEach((weather) => {
    if (daysWithIcons[i].icons[weather.icon]) {
      daysWithIcons[i].icons[weather.icon] += 1;
    } else {
      daysWithIcons[i].icons[weather.icon] = 1;
    }
  });
  return daysWithIcons;
};

const selectPredominantIcon = (days) => {
  return days.map((day) => {
    day.icons = Object.keys(day.icons).reduce((acc, curr) => {
      if (!curr.includes('n')) {
        if (acc === null || day.icons[curr] > day.icons[acc]) {
          return curr;
        }
      }
      return acc;
    }, null);
    return day;
  });
};

const capitalize = (words) => {
  return words.split(' ')
    .map((word) => {
      return `${word.slice(0, 1).toUpperCase()}${word.slice(1)}`;
    })
    .join(' ');
};

const format = (results) => {
  let days = newDays();
  const { list } = results.data;
  let currentDate = getDate(list[0]);
  days[0].date = currentDate;
  let i = 0;
  list.forEach((dataset) => {
    if (getDate(dataset) !== currentDate) {
      i += 1;
      if (i < 5) {
        currentDate = getDate(dataset);
        days[i].date = currentDate;
      }
    }
    if (i < 5) {
      days = updateTemps(days, dataset, i);
      days = updateIcons(days, dataset, i);
    }
  });
  days = selectPredominantIcon(days);
  days[5] = {
    city: results.data.city.name,
    temp: Math.round(list[0].main.temp),
    humidity: Math.round(list[0].main.humidity),
    main: capitalize(list[0].weather[0].description),
  };
  return days;
};

module.exports = (cityName) => {
  return axios.get(`http://api.openweathermap.org/data/2.5/forecast?q=${cityName},us&units=imperial&APPID=${API_KEY}`)
    .then((results) => {
      return format(results);
    })
    .catch((error) => {
      console.log('error @ getWeatherData', error);
    });
};
