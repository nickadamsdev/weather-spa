module.exports = [{
  date: 'Apr 25', icons: '01d', maxTemp: 78, minTemp: 47,
}, {
  date: 'Apr 26', icons: '01d', maxTemp: 76, minTemp: 48,
}, {
  date: 'Apr 27', icons: '01d', maxTemp: 74, minTemp: 43,
}, {
  date: 'Apr 28', icons: '02d', maxTemp: 68, minTemp: 42,
}, {
  date: 'Apr 29', icons: '10d', maxTemp: 68, minTemp: 41,
}, { 
  city: 'Seattle', temp: 49, humidity: 63, main: 'Overcast Clouds',
}];
