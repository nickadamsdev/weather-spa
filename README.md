# Nick Adams Coding Challenge
## Welcome
Thanks for taking a look at my coding challenge! I hope you have as much fun reviewing it as I did writing it.
A few things I would change in production:

 - [ ] Build bundle.js in production mode with webpack for minified version
 - [ ] Adjust day beginning and end to local time so that data more accurately reflects the weather experienced during the local 24 hour period
 - [ ] A couple of animations to add interest
 - [ ] Caching in a database to minimize repeated API calls. Each dataset would decay by least used and each hour

##  Installation
After unzipping, assuming node 9 is installed, in the root folder run: 

    npm install
Then start the server by running:

    npm start
That's it! Navigate to http://localhost:8080 to test.