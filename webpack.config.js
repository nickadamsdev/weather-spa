const path = require('path');

module.exports = {
  context: path.resolve(__dirname),
  entry: './client/index.jsx',
  output: {
    path: path.resolve(__dirname + '/public'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        loader: 'babel-loader',
        test: /.jsx$/,
        exclude: '/node_modules/',
        options: {
          presets: ['env', 'react']
        }
      }
    ]
  }
};
