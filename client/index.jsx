import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faSearch from '@fortawesome/fontawesome-free-solid/faSearch';
import Weather from './Weather.jsx';
import url from '../url';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 'land',
      city: '',
      weatherData: null,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.reset = this.reset.bind(this);
  }
  reset() {
    this.setState({ page: 'land' });
  }
  handleChange(event) {
    this.setState({ city: event.target.value });
  }
  handleKeyPress(event) {
    if (event.key === 'Enter') {
      this.handleSearch();
    }
  }
  handleSearch() {
    axios
      .get(`${url}/api/${this.state.city}`)
      .then(results => {
        this.setState({
          page: 'weather',
          weatherData: results.data,
        });
      })
      .catch(error => {
        console.log(error);
      });
  }
  render() {
    if (this.state.page === 'land') {
      return (
        <div className="search">
          <div className="searchInputIcon">
            <input
              autoFocus
              value={this.state.city}
              className="searchInput"
              onChange={this.handleChange}
              onKeyPress={this.handleKeyPress}
              placeholder="Type city here"
              type="text"
            />
            <FontAwesomeIcon
              onClick={this.handleSearch}
              icon={faSearch}
              className="searchIcon"
            />
          </div>
        </div>
      );
    }
    return <Weather weatherData={this.state.weatherData} reset={this.reset} />;
  }
}

ReactDOM.render(<App />, document.getElementById('app'));
