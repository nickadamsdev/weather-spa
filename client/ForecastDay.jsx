import React from 'react';

const ForecastDay = (props) => {
  let dayClass = 'forecastDay rightBorder';
  if (props.id === 4) {
    dayClass = 'forecastDay';
  }
  const imgSrc = `http://openweathermap.org/img/w/${props.data.icons}.png`;
  return (
    <div className={dayClass}>
      <div className="forecastDate">{props.data.date}</div>
      <img className="forecastIcon" src={imgSrc} />
      <div className="forecastMax">{props.data.maxTemp}</div>
      <div className="forecastMin">{props.data.minTemp}</div>
    </div>
  );
};

export default ForecastDay;
