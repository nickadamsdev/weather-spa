import React from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faSearch from '@fortawesome/fontawesome-free-solid/faSearch';
import faTimes from '@fortawesome/fontawesome-free-solid/faTimes';
import ForecastDay from './ForecastDay.jsx';

const Weather = (props) => {
  let today = props.weatherData[5];
  let forecast = props.weatherData.slice(0, 5);
  return (
    <div className="weather">
      <div className="weatherTitle">
        <FontAwesomeIcon icon={faSearch} className="icon" />
        <span className="cityName">{today.city}</span>
        <FontAwesomeIcon onClick={props.reset} icon={faTimes} className="icon topright" />
      </div>
      <div className="now">
        <div className="now-temp">{today.temp}&deg;</div>
        <div className="now-info">
          <div className="F">F</div>
          <div className="main">{today.main}</div>
          <div className="humidity">{today.humidity}% Humidity</div>
        </div>
      </div>
      <div className="forecast">
        {forecast.map((data, index) => {
          return <ForecastDay data={data} key={index} id={index} />;
        })}
      </div>
    </div>
  );
};

export default Weather;
