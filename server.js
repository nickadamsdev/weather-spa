const express = require('express');
const getWeatherData = require('./data/getWeatherData');
// const fakeData = require('./data/fakeData');
const PORT = process.env.PORT || 8080;

const app = express();
app.use(express.static('public'));
app.get('/api/:city', (req, res) => {
  getWeatherData(req.params.city)
    .then(results => {
      res.send(JSON.stringify(results));
    })
    .catch(error => {
      console.log('error @ server', error);
    });
});
// uncomment below to get fake data - replaces above app.get()
// app.get('/api/:city', (req, res) => {
//   res.send(JSON.stringify(fakeData));
// });

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
